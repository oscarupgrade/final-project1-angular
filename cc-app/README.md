
Instalación / Arranque Proyecto:
Levantar el back-api

Sitúate con la terminal en la carpeta back-api.
Ejecutar:
npm i json-server (la primera vez que lo abrimos)
npm run server


Levantar el proyecto:

Sitúate en la carpeta del proyecto
Situate en la carpeta cc-app
Ejecuta npm i (solo la primera vez)
Ejecuta ng serve
Navega a http://localhost:4200/. La app se cargará automáticamente si haces algun cambio.


AngularLazyLoading

Hemos trabajado con LazyLoading, de esta manera la carga de archivos pesará menos en la app. Este proyecto ha sido generado con Angular CLI version 10.0.4.


Éste proyecto es el primero que hago desde cero, con lo aprendido en clase,
siguiendo los pasos que nos marcó el profesor.

Es una página en el que he querido plasmar la idea que tenía de un cento comercial y
mostrar las diferentes opciones que tiene el usuario navegando por ella. 

Éste proyecto está anidado a un back-api json-server, que nos sirve para añadir los datos que queramos, que se verán en nuestra aplicación. En el que podemos recuperar esos datos a través de Angular.

En el proyecto teníamos una serie de requisitos mínimos:

- Una Home con dos componentes
- Una pagina con un detail que vaya a otra pagina
- Header y Footer
- Un About

En el proyecto he usado metodología BEM, y librerías externas como son: 

-ngx-swiper-wrapper
-ngx-translate

Más cosas añadidas al proyecto para que fuese un poco mas completo, es un mapa en el que localizamos el supuesto centro comercial, un search para encontrar el restaurante que queramos dentro de la lista que tenemos y un formulario en el que nos añade ese nuevo registro al db.json y aparte nos pinte los datos que le pasamos en la misma página.

Y por último pero no menos importante comentar el uso del botón de ir hacia atrás, el uso de enlaces a páginas oficiales, la forma de maquetar y tener un código más limpio.










<!-- # CcApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page. -->
