export interface OcioList{
    id: number;
    name: string;
    image: string;

// tslint:disable-next-line: eofline
}

export interface OcioListDetail{
    id: number;
    name: string;
    image: string;
    gallery: string[];
    description: string;

// tslint:disable-next-line: eofline
}

export interface RestauracionList{
    id: number;
    name: string;
    image: string;
// tslint:disable-next-line: eofline
}

export interface ShoppingList{
    id: number;
    name: string;
    image: string;
    tienda: string;
// tslint:disable-next-line: eofline
}

export interface ShoppingDetail{
    id: number;
    name: string;
    image1: string;
    image2: string;
    history: string;
// tslint:disable-next-line: eofline
}