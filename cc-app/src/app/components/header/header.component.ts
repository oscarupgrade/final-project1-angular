import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
// tslint:disable-next-line: no-inferrable-types
public logo: string =  'http://infolocal.comfenalcoantioquia.com/media/com_jbusinessdirectory/pictures/companies/73/aventuracentrocomercial-1487881164.png';

// tslint:disable-next-line: no-inferrable-types
public logoAlt: string = 'Logo centro comercial';

 constructor() { }

  ngOnInit(): void {
  }



}
