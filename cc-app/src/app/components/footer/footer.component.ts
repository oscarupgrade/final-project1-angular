import { Component, OnInit } from '@angular/core';
import { Footer } from './models/Ifooter';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

public footer: Footer | any = {};
horario: string[];
centro: string[];
contact: string[];
  constructor() {

    this.horario = ['Horario Restauración: Lunes a Domingo 10:00 a 01:30 h.', 'Horario Aparcamiento: Lunes a Domingo 09:00 a 01:00 h', 'Horario Comercial: Lunes a Domingo: 10:00 a 22:00h'];
    this.centro = ['Trabaja con nosotros', 'Tu negocio aquí', 'Publicidad', 'Servicios para ti'];
    this.contact = ['Tlf: 679555555', 'Email: informacion@avantia.com', 'Dirección: Avd Nicaragua 25, Móstoles'];
    console.log(this.footer);
    console.log(this.horario, this.centro, this.contact);
  }

  ngOnInit(): void {
    console.log('oninit');
    this.footer = {
      horario: this.horario,
      centro: this.centro,
      contact: this.contact
    };
    console.log(this.footer);
  }

}
