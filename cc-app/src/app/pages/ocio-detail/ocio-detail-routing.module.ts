import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OcioDetailComponent } from './ocio-detail-component/ocio-detail.component';


const routes: Routes = [{path: '', component: OcioDetailComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OcioDetailRoutingModule { }
