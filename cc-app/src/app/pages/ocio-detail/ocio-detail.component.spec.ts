import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OcioDetailComponent } from './ocio-detail-component/ocio-detail.component';

describe('OcioDetailComponent', () => {
  let component: OcioDetailComponent;
  let fixture: ComponentFixture<OcioDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OcioDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OcioDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
