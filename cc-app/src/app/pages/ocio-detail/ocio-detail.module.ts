
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OcioDetailRoutingModule } from './ocio-detail-routing.module';
import { OcioDetailComponent } from './ocio-detail-component/ocio-detail.component';
import { OcioService } from 'src/app/services/ocio-app/ocio.service';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [OcioDetailComponent],
  imports: [
    CommonModule,
    OcioDetailRoutingModule,
    TranslateModule
  ],
  providers: [OcioService],
})
export class OcioDetailModule {}
