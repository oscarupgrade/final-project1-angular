
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';


import {OcioService} from '../../../services/ocio-app/ocio.service';
import { OcioListDetail } from './../../../models/ocio-interface';


@Component({
  selector: 'app-ocio-detail',
  templateUrl: './ocio-detail.component.html',
  styleUrls: ['./ocio-detail.component.scss']
})
export class OcioDetailComponent implements OnInit {
  public ocioDetail: OcioListDetail | any = {};
  public ocioDetailId: string | null = null;

  constructor(private ocioService: OcioService, private route: ActivatedRoute, private location: Location) { }

    ngOnInit(): void {

    this.getOcioDetail();
  }

   public getOcioDetail(): void{
    this.route.paramMap.subscribe((params) => {
      this.ocioDetailId = params.get('id');
    });

    this.ocioService.getOcioDetail(Number(this.ocioDetailId)).subscribe(
      (data: any) => {
        this.ocioDetail = data;

      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public goBack(): void{
    this.location.back();
  }
}
