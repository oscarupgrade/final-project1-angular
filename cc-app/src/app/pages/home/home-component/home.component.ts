import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public slider = [
    'assets/img/home/interior.jpg',
    'assets/img/home/sorteo.jpg',
    'https://i.ytimg.com/vi/Ootau-awtos/maxresdefault.jpg',
    'https://navidad.es/wp-content/uploads/2018/10/ideas-originales-y-divertidas-para-los-eventos-navidenos-para-empresas-7.png'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
