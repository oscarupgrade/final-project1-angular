import { Gallery, Photos } from './models/Igallery';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-part2',
  templateUrl: './part2.component.html',
  styleUrls: ['./part2.component.scss']
})
export class Part2Component implements OnInit {

  Part2: Gallery | any = {};
  Photo1: Photos | any = {};
  Photo2: Photos | any = {};
  Photo3: Photos | any = {};
  Photo4: Photos | any = {};

  constructor() {
    this.Photo1.img = 'assets/img/home/evento.jpg';
    this.Photo1.imgAlt = 'Imagen evento';

    this.Photo2.img = 'assets/img/home/sorteo.jpg';
    this.Photo2.imgAlt = 'Imagen evento';

    this.Photo3.img = 'assets/img/home/noticias.jpg';
    this.Photo3.imgAlt = 'Imagen evento';

    this.Photo4.img = 'assets/img/home/ofertas.jpg';
    this.Photo4.imgAlt = 'Imagen evento';
   }

  ngOnInit(): void {
  }

}
