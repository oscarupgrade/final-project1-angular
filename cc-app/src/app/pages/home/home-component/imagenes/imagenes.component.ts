import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { SwiperComponent, SwiperDirective, SwiperConfigInterface,
SwiperScrollbarInterface, SwiperPaginationInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-imagenes',
  templateUrl: './imagenes.component.html',
  styleUrls: ['./imagenes.component.scss']
})
export class ImagenesComponent implements OnInit {

  @Input() slider: [] | any;

  // tslint:disable-next-line: no-inferrable-types
  public show: boolean = true;
  // tslint:disable-next-line: no-inferrable-types
  public type: string = 'component';
  // tslint:disable-next-line: no-inferrable-types
  public disabled: boolean = false;

   public config: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: false,
    scrollbar: false,
    navigation: true,
    pagination: false,
    autoplay: {delay: 6000, stopOnLastSlide: false, reverseDirection: false, disableOnInteraction: false}
  };

  private scrollbar: SwiperScrollbarInterface = {
    el: '.swiper-scrollbar',
    hide: false,
    draggable: true
  };

  private pagination: SwiperPaginationInterface = {
    el: '.swiper-pagination',
    clickable: true,
    hideOnClick: false
  };

  @ViewChild(SwiperComponent) componentRef: SwiperComponent | undefined;
  @ViewChild(SwiperDirective) directiveRef: SwiperDirective | undefined;

  constructor() { }

// tslint:disable-next-line: typedef
public toggleType() {
    this.type = (this.type === 'component') ? 'directive' : 'component';
  }

  // tslint:disable-next-line: typedef
  public toggleDisabled() {
    this.disabled = !this.disabled;
  }

  // tslint:disable-next-line: typedef
  public toggleDirection() {
    this.config.direction = (this.config.direction === 'horizontal') ? 'vertical' : 'horizontal';
  }

  // tslint:disable-next-line: typedef
  public toggleSlidesPerView() {
    if (this.config.slidesPerView !== 1) {
      this.config.slidesPerView = 1;
    } else {
      this.config.slidesPerView = 2;
    }
  }

  // tslint:disable-next-line: typedef
  public toggleOverlayControls() {
    if (this.config.navigation) {
      this.config.scrollbar = false;
      this.config.navigation = false;

      this.config.pagination = this.pagination;
    } else if (this.config.pagination) {
      this.config.navigation = false;
      this.config.pagination = false;

     // tslint:disable-next-line: align
     this.config.scrollbar = this.scrollbar;
    } else {
      this.config.scrollbar = false;
      this.config.pagination = false;

      this.config.navigation = true;
    }

    // if (this.type === 'directive') {
    //   this.directiveRef.setIndex(0);
    // } else {
    //   this.componentRef.directiveRef.setIndex(0);
    // }
  }

  // tslint:disable-next-line: typedef
  public toggleKeyboardControl() {
    this.config.keyboard = !this.config.keyboard;
  }

  // tslint:disable-next-line: typedef
  public toggleMouseWheelControl() {
    this.config.mousewheel = !this.config.mousewheel;
  }

  // tslint:disable-next-line: typedef
  public onIndexChange(index: number) {
    console.log('Swiper index: ', index);
  }

  ngOnInit(): void {
  }

}
