
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { ImagenesComponent } from './home-component/imagenes/imagenes.component';
import { Part2Component } from './home-component/part2/part2.component';

import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

import { TranslateModule } from '@ngx-translate/core';


const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 0,
  slidesPerView: 1,
  centeredSlides: true
};

@NgModule({
  declarations: [HomeComponent, ImagenesComponent, Part2Component],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SwiperModule,
    TranslateModule
    
  ],
    providers: [{
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG
  }]
})
export class HomeModule { }
