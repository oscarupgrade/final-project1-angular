import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

import { ShoppingService } from 'src/app/services/shopping-app/shopping.service';
import { ShoppingList } from './../../../models/ocio-interface';

@Component({
  selector: 'app-shopping',
  templateUrl: './shopping.component.html',
  styleUrls: ['./shopping.component.scss']
})
export class ShoppingComponent implements OnInit {

  public shoppingList: ShoppingList[] = [];

  constructor(private shoppinService: ShoppingService, private location: Location) { }

  ngOnInit(): void {
    this.getShoppingList();
  }

  public getShoppingList(): void{
    this.shoppinService.getShoppingList().subscribe(
      (data: ShoppingList[]) => {
        this.shoppingList = data;
      },
      (err) => {
        console.error(err.message);
      }
    );

  }

  public goBack(): void{
    this.location.back();
    }

}
