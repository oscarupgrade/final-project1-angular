import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { ShoppingRoutingModule } from './shopping-routing.module';
import { ShoppingComponent } from './shopping-component/shopping.component';


@NgModule({
  declarations: [ShoppingComponent],
  imports: [
    CommonModule,
    ShoppingRoutingModule,
    TranslateModule
  ]
})
export class ShoppingModule { }
