import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OcioRoutingModule } from './ocio-routing.module';
import { OcioComponent } from './ocio-component/ocio.component';

import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [OcioComponent],
  imports: [
    CommonModule,
    OcioRoutingModule,
    TranslateModule
  ]
})
export class OcioModule { }
