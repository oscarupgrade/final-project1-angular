import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OcioComponent} from './ocio-component/ocio.component';

const routes: Routes = [{path: '', component: OcioComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OcioRoutingModule { }
