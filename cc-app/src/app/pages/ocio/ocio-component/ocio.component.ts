
import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';

import { OcioService } from 'src/app/services/ocio-app/ocio.service';

import { OcioList } from './../../../models/ocio-interface';


@Component({
  selector: 'app-ocio',
  templateUrl: './ocio.component.html',
  styleUrls: ['./ocio.component.scss']
})
export class OcioComponent implements OnInit {

  public ocioList: OcioList[] = [];

  constructor(private ocioService: OcioService, private location: Location) { }

  ngOnInit(): void {
    this.getOcioList();
  }

  public getOcioList(): void{
  this.ocioService.getOcioList().subscribe(
    (data: OcioList[]) => {
      this.ocioList = data;
    },
    (err) => {
      console.error(err.message);
    }
  );
}

    public goBack(): void{
    this.location.back();
    }
}
