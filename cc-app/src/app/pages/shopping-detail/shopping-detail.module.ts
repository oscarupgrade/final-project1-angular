
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { ShoppingDetailRoutingModule } from './shopping-detail-routing.module';
import { ShoppingDetailComponent } from './shopping-detail-component/shopping-detail.component';
import { ShoppingService } from 'src/app/services/shopping-app/shopping.service';


@NgModule({
  declarations: [ShoppingDetailComponent],
  imports: [
    CommonModule,
    ShoppingDetailRoutingModule,
    TranslateModule
  ],
  providers: [ShoppingService],
})
export class ShoppingDetailModule { }
