
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';


import { ShoppingDetail } from 'src/app/models/ocio-interface';
import { ShoppingService } from './../../../services/shopping-app/shopping.service';


@Component({
  selector: 'app-shopping-detail',
  templateUrl: './shopping-detail.component.html',
  styleUrls: ['./shopping-detail.component.scss']
})
export class ShoppingDetailComponent implements OnInit {

  public shoppingDetail: ShoppingDetail | any = {};
  public shoppingDetailId: string | null = null;

  constructor(private shoppingService: ShoppingService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit(): void {
    this.getShoppingDetail();
  }

  public getShoppingDetail(): void{
    this.route.paramMap.subscribe((params) => {
      this.shoppingDetailId = params.get('id');
    });

    this.shoppingService.getShoppingDetail(Number(this.shoppingDetailId)).subscribe(
      (data: any) => {
        this.shoppingDetail = data;

      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public goBack(): void{
    this.location.back();
  }

}
