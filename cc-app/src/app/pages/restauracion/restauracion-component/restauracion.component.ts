
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit,Output,Input,EventEmitter } from '@angular/core';
import {Location} from '@angular/common';

import { RestauracionService } from 'src/app/services/restauracion-app/restauracion.service';
import { RestauracionList } from './../../../models/ocio-interface';

@Component({
  selector: 'app-restauracion',
  templateUrl: './restauracion.component.html',
  styleUrls: ['./restauracion.component.scss']
})
export class RestauracionComponent implements OnInit {

  public restauracionList: RestauracionList[] = [];
  public myForm: FormGroup | any = null;
  public submitted: boolean = false;

  @Input() public showModal: boolean = false;
  
  

  constructor(private restauracionService: RestauracionService, private location: Location, private formBuilder: FormBuilder ) { }

  ngOnInit(): void {
    this.getRestauracionList();

    this.createFakeForm(); 
    
  }

  public getRestauracionList(): void{
    this.restauracionService.getRestauracionList().subscribe(
      (data: RestauracionList[]) => {
        this.restauracionList = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
    
  }

  public goBack(): void{
    this.location.back();
  }

  public createFakeForm(): void{
    this.myForm = this.formBuilder.group({
    id: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(20)]],
    name:  ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
    image: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
    });
  }

  public onSubmit(): void{
    this.submitted = true;
    this.showModal = true;
    if(this.myForm.valid) {
      this.postFake();
      this.myForm.reset();
      this.submitted = false;
    }
  }

  public postFake(): void{
    const myFake: any = {
      id: this.myForm.get('id').value,
      name: this.myForm.get('name').value,
      image: this.myForm.get('image').value
    };
    this.restauracionService.postFakeList(myFake).subscribe((data) => {console.log(data);},
    (err) => console.error(err.message));
  }



}
