import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchRestauracionComponent } from './search-restauracion.component';

describe('SearchRestauracionComponent', () => {
  let component: SearchRestauracionComponent;
  let fixture: ComponentFixture<SearchRestauracionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchRestauracionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchRestauracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
