import { RestauracionList } from './../../../../../models/ocio-interface';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';



@Component({
  selector: 'app-search-restauracion',
  templateUrl: './search-restauracion.component.html',
  styleUrls: ['./search-restauracion.component.scss']
})
export class SearchRestauracionComponent implements OnInit {

  countryList: string[];
  filteredList: string[];
  filter: string;
  public RestauracionList: any = {};

  // @Output('search') searchEmitter = new EventEmitter<string>();
  // search = new FormControl('');

  constructor() {
    this.filter = '';
    this.countryList = ['Foster Hollywood', 'Buffalo', 'Tgb', 'Burguer King', 'McDonalds', 'Taco Bell',
     'Tagliatella', 'KFC', 'Telepizza','Vips','Ginos','Brasa y Leña','Tommy Mels','Goiko Grill','Muerde la pasta'];
		this.filteredList = this.countryList;  
   }

  ngOnInit(): void {
    // this.search.valueChanges.pipe(debounceTime(300)).subscribe(value => this.searchEmitter.emit(value))
  }

  onChangeFilter(filter: string) {
    const newList: string[] = this.countryList.filter(el => el.toLowerCase().includes(filter.trim().toLowerCase()));
    this.filteredList = newList;
  }
}
  

  


