import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalRestauracionComponent } from './modal-restauracion.component';

describe('ModalRestauracionComponent', () => {
  let component: ModalRestauracionComponent;
  let fixture: ComponentFixture<ModalRestauracionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalRestauracionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalRestauracionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
