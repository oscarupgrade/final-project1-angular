import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';


import { RestauracionRoutingModule } from './restauracion-routing.module';
import { RestauracionComponent } from './restauracion-component/restauracion.component';
import { ModalRestauracionComponent } from './restauracion-component/modal-restauracion/modal-restauracion.component';
import { SearchRestauracionComponent } from './restauracion-component/search-restauracion/search-restauracion/search-restauracion.component';
import { SearchPipe } from 'src/app/pipe/search.pipe';


@NgModule({
  declarations: [RestauracionComponent, ModalRestauracionComponent, SearchRestauracionComponent],
  imports: [
    CommonModule,
    RestauracionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule
    
  ]
})
export class RestauracionModule { }
