import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RestauracionComponent} from './restauracion-component/restauracion.component';

const routes: Routes = [{path: '', component: RestauracionComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestauracionRoutingModule { }
