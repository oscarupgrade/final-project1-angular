import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule)},
  {path: 'shopping', loadChildren: () => import('./pages/shopping/shopping.module').then((m) => m.ShoppingModule)},
  {path: 'ocio', loadChildren: () => import('./pages/ocio/ocio.module').then((m) => m.OcioModule)},
  {path: 'restauracion', loadChildren: () => import('./pages/restauracion/restauracion.module').then((m) => m.RestauracionModule)},
  {path: 'contact', loadChildren: () => import('./pages/contact/contact.module').then((m) => m.ContactModule)},
  {path: 'ocio-detail/:id', loadChildren: () => import('./pages/ocio-detail/ocio-detail.module').then((m) => m.OcioDetailModule)},
  {path: 'shopping-detail/:id',
   loadChildren: () => import('./pages/shopping-detail/shopping-detail.module').then((m) => m.ShoppingDetailModule)},
  {path: 'map', loadChildren: () => import('./pages/map/map.module').then((m) => m.MapModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
