import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import {ShoppingList, ShoppingDetail} from './../../models/ocio-interface';


@Injectable({
  providedIn: 'root'
})
export class ShoppingService {

  private shoppingListUrl = 'http://localhost:3000/ShoppingList';
  private shoppingDetailUrl = 'http://localhost:3000/ShoppingDetail';

  constructor(private http: HttpClient) { }

  public getShoppingList(): Observable<ShoppingList[]>{
    return this.http.get(this.shoppingListUrl).pipe(
      map((response: any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
   // tslint:disable-next-line: semicolon
   )
  }


  public getShoppingDetail(id: number): Observable<ShoppingDetail>{
    return this.http.get(`${this.shoppingDetailUrl}/${id}`).pipe(
      map((response: any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{

          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
   // tslint:disable-next-line: semicolon
   );
  }
}
