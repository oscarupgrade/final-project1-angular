
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { OcioList, OcioListDetail} from './../../models/ocio-interface';


@Injectable({
  providedIn: 'root'
})
export class OcioService {

  private ocioListUrl = 'http://localhost:3000/OcioList';
  private ocioListDetailUrl = 'http://localhost:3000/OcioDetail';

  constructor(private http: HttpClient) {}

   public getOcioList(): Observable<OcioList[]>{
    return this.http.get(this.ocioListUrl).pipe(
      map((response: any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
   // tslint:disable-next-line: semicolon
   )
  }

  public getOcioDetail(id: number): Observable<OcioListDetail>{
    return this.http.get(`${this.ocioListDetailUrl}/${id}`).pipe(
      map((response: any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{

          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
   // tslint:disable-next-line: semicolon
   );
  }


}
