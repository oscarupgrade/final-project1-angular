import { TestBed } from '@angular/core/testing';

import { OcioService } from './ocio.service';

describe('OcioService', () => {
  let service: OcioService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OcioService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
