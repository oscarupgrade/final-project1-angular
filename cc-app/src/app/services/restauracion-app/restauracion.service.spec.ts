import { TestBed } from '@angular/core/testing';

import { RestauracionService } from './restauracion.service';

describe('RestauracionService', () => {
  let service: RestauracionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RestauracionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
