import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import {RestauracionList} from './../../models/ocio-interface';

@Injectable({
  providedIn: 'root'
})
export class RestauracionService {

  private restauracionListUrl = 'http://localhost:3000/RestauracionList';

  constructor(private http: HttpClient) { }

  public getRestauracionList(): Observable<RestauracionList[]>{
    return this.http.get(this.restauracionListUrl).pipe(
      map((response: any) => {
        if (!response){
          throw new Error('Value expected!');
        }else{
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
   // tslint:disable-next-line: semicolon
   )
  }

  public postFakeList(fakelist: any): Observable<any> {
    return this.http.post(this.restauracionListUrl, fakelist).pipe(
      map((response: any) => {
        if(!response){
          throw new Error('Value expected!');
        } else {
          console.log(response);
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })

    );
  }
}
